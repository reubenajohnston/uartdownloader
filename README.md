# uartdownloader
This is a python downloader program for extracting files for research purposes.  This program will dd (5MB chunks at a time) from a specified device on a uart connection and download it to your local PC using zmodem.  Once all the chunks are downloaded, it concatenates them into a single binary.  The original mode that I coded, called func1, used a command file that contains all the commands for the sequence described above.  The latest mode, called func2, auto-generates the command file for you, based on a few key parameters.  

Currently, the output appears better using a Debug session in Eclipse.  I have not diagnosed why a large portions of the messages do not appear when running from a terminal.

# Examples
Mode func1 (for downloading contents of /dev/acta over /dev/ttyUSB0 in 5MB chunks)

```$ python3 uartdownloader.py --uartDevice=/dev/ttyUSB0 --commandFile=/mnt/hgfs/Sandbox/uartdownloader/commandfile.txt --mode=func1 --debug```

Mode func2 (for downloading contents of /dev/acta over /dev/ttyUSB0 in 5MB chunks)

```$ python3 uartdownloader.py --uartDevice=/dev/ttyUSB0 --commandFile=/mnt/hgfs/Sandbox/uartdownloader/commandfile.txt --dumpDevice=/dev/acta --dumpSize=20971520 --dumpChunkSize=5M --dumpStartIndexChunk=0 --mode=func2 --dumpOutputDir=~/Downloads --dumpOutputName=acta.bin --debug```

Mode func2 (for downloading contents of /dev/actb over /dev/ttyUSB0 in 5MB chunks)

```$ python3 uartdownloader.py --uartDevice=/dev/ttyUSB0 --commandFile=/mnt/hgfs/Sandbox/uartdownloader/commandfile.txt     --dumpDevice=/dev/actb --dumpSize=157286400 --dumpChunkSize=5M --dumpStartIndexChunk=0 --mode=func2 --dumpOutputDir=~/Downloads --dumpOutputName=actb.bin --debug```
