import sys
import time
import traceback
import pexpect
    
def open_client(debug):
    try:
        child=pexpect.spawn("bash -i")
    
        sessionDict=dict([("child",child)])
    
        if(debug):
            print("open_client() "+str(sessionDict))
        
        return(sessionDict)
    except Exception as e:
        print("Unable to connect to child")
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        sys.exit(1) #panic

def close_client(sessionDict, debug):
    try:
        child=sessionDict["child"]
        child.close()
        sessionDict=None
        if(debug):
            print("close_client()")       
        return(sessionDict)
    except Exception as e:
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        sys.exit(1) #panic

def serial_write(child, bytecommand, debug):    
    if debug:
        print("sending: \n"+bytecommand.decode("utf8",errors="ignore"))
    child.write(bytecommand)
    
def command_client(sessionDict, sequencestep, debug):
    #sends a single command
    child=sessionDict["child"]
    
    stdout=""
    if sequencestep["type"]=="send":
        bytecommand=(sequencestep["arg1"]+"\n").encode("utf8")
#        if not(child.before is None):
#            stdout=child.after.decode("utf8")+"\n" #values received since the last expect
        serial_write(child, bytecommand, debug)        
    elif sequencestep["type"]=="expect":
        #loop to continue after timeout exception
        if debug:
            if sequencestep["arg1"][0]=="[":
                temp=sequencestep["arg1"][1:-1]#trim off the leading '[' and trailing ']'
            else:
                temp=sequencestep["arg1"]
            print("Waiting for: %s" % (temp))                    
        while True:
            if sequencestep["arg1"][0]=="[":
                try:
                    child.expect(sequencestep["arg1"])                    
                except pexpect.exceptions.TIMEOUT:
                    if debug:
                        print("Received: \n"+child.buffer.decode("utf8",errors="ignore")+"\n")#values received since the last expect
                    continue
            else:
                try:                
                    child.expect_exact(sequencestep["arg1"])
                except pexpect.exceptions.TIMEOUT:
                    if debug:
                        print("Received: \n"+child.buffer.decode("utf8",errors="ignore")+"\n")#values received since the last expect
                    continue                    
            stdout=child.before.decode("utf8",errors="ignore")+temp+"\n"

            if debug:
                print("Received: \n"+stdout)
            break  
    elif sequencestep["type"]=="sleep":
        if debug:
            print("Sleeping "+sequencestep["arg1"]+" seconds\n")
        time.sleep(int(sequencestep["arg1"]))
    else:
        raise
  
    return(stdout)                

def command_handler(session,function,sequencestep,debug,sessionDict):    
    if (session==False): #we are in single command mode
        sessionDict=open_client(debug)
        stdout=command_client(sessionDict, sequencestep, debug)
        sessionDict=close_client(sessionDict, debug)
    else: #we are in session mode
        #if debug:
        #    print("session mode")
        if (function=="open_client"):
            sessionDict=open_client(debug)
        elif (function=="command_client"):
            stdout=command_client(sessionDict,sequencestep,debug)
        elif (function=="close_client"):
            sessionDict=close_client(sessionDict, debug)
    return(sessionDict)
    
    