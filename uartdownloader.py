import argparse
import csv
import ctypes
import sys
import tty_device
import tty_local

# Command file 
#   Syntax is based on CSV DictReader
#   X,valid_terminal_command,comment
# Fields
#   1) X is the sequence id 0-N
#   2) endpoint (LOCAL, REMOTE)
#   3) valid_terminal_command is string with one of:
#     1) terminal command (no commas)
#     2) 0x1A, which is SUBSTITUTE, or ctrl-z (note, this will auto-prompt and wait for "Suspended")
#     3) 0x03, which is end-of-text, or ctrl-c (note, this will auto-prompt and wait for "^C")
#     4) 0x0A, which is line feed, or \n (note, this will auto-prompt and wait for "#")
#     5) #, which will pend receiving on a prompt
#   4) comment

# Download sequence:
#   1) remote setup
#   2) local setup
#   3) local receive
#   4) remote send
#   5) remote teardown
#   6) local teardown

# Upload sequence:
#   1) local setup
#   2) remote setup
#   3) remote receive
#   4) local send
#   5) local teardown
#   6) remote teardown

def usage():
    print("Usage : %s device commandfile" % sys.argv[0])
    sys.exit(0)

def genListChunk(dumpSize, chunkSize, dumpStartIndexChunk):
    #id,size,byte_start
    fullListChunks=[]
    
    #convert k to kB
    #convert M to MB
    libc=ctypes.CDLL("libc.so.6")
    temp1=ctypes.c_int()
    temp2=ctypes.create_string_buffer(b'\000' * 128)
    libc.sscanf(chunkSize.encode("utf8"),b"%d%s",ctypes.byref(temp1),temp2)

    if temp2.value.decode("utf8")=="k":
        intChunkSize=temp1.value*1024
    elif temp2.value.decode("utf8")=="M":
        intChunkSize=temp1.value*1048576        
    else:
        raise
    
    intDumpSize=int(dumpSize)
    
    temp=intDumpSize%intChunkSize
    dumpNumChunks=int((intDumpSize+temp)/intChunkSize)
    
    for i in range(int(dumpStartIndexChunk),dumpNumChunks):
        listEntry={}
#todo: need to make id 2 digits 00,01,02,...,98,99
        listEntry["id"]=i
        listEntry["size"]=intChunkSize
        listEntry["byte_start"]=i*intChunkSize
        fullListChunks.append(listEntry)

    return(fullListChunks)
    
def appendSequence(sequenceList, seqStr):
    seqEntry={}
    seqStrList=seqStr.split(",")
    seqEntry["sequenceid"]=seqStrList[0]
    seqEntry["endpoint"]=seqStrList[1]
    seqEntry["type"]=seqStrList[2]
    seqEntry["arg1"]=seqStrList[3]
    if len(seqStrList)==5:
        seqEntry["comment"]=seqStrList[4]
    
    sequenceList.append(seqEntry)
    return(sequenceList)
    
def runSequence(uartDevice, uartBaudRate, sequenceList, debug):
    if debug:
        print("Sequence:")
        for idxSequence, sequenceStep in enumerate(sequenceList):
            print("\t%s" % (sequenceStep),flush=True)
    
    sessionDictRemote=tty_device.command_handler(uartDevice, uartBaudRate, True, "open_client", None, debug, None)
    sessionDictLocal=tty_local.command_handler(True, "open_client", None, debug, None)
    
    session=True
    
    #iterate over sequence
    for idxSequence, sequenceStep in enumerate(sequenceList):
        #iterate over states
        if debug:
            print("\tsequencestep[%d]=%s\n" % (idxSequence, sequenceStep), flush=True)

        if sequenceStep["endpoint"]=="REMOTE":
            sessionDictRemote=tty_device.command_handler(uartDevice, uartBaudRate, session, "command_client", sequenceStep, debug, sessionDictRemote)
        else:
            sessionDictLocal=tty_local.command_handler(session,"command_client",sequenceStep,debug,sessionDictLocal)

    sessionDictRemote=tty_device.command_handler(uartDevice, uartBaudRate, session, "close_client", None, debug, sessionDictRemote)
    sessionDictLocal=tty_local.command_handler(session, "close_client", None, debug, sessionDictLocal)

def genSequence(uartDevice, uartBaudRate, dumpDevice, listChunks, outputDir, outputName):
    count=1
    #setup
    sequenceList=[]
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,\x0A,//send newline')
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,\x0A,//send newline')
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,export DEV='+uartDevice)
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')
    count=count+1
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,stty -F $DEV '+uartBaudRate+',//setup stty') 
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],wait for prompt')
    count=count+1                                                                                  
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,cd '+outputDir+',//cd into the output directory') 
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],wait for prompt')
    count=count+1                                                                                  

    for idx_row, row in enumerate(listChunks):
        filename=outputDir+'/'+outputName
        tmpfilename="/tmp/"+outputName
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,dd if='+dumpDevice+' of='+tmpfilename+'.'+str(row["id"])+' bs=1 skip='+str(row["byte_start"])+' count='+str(row["size"]))        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,lsz -b -y '+tmpfilename+'.'+str(row["id"])+',//terminate a hung lsz by ctrl-x')       
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,sleep,2,//sleep 2 seconds')       
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,rz -b > $DEV < $DEV')       
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,sleep,1,//sleep 1 second')       
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,Transfer complete')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,\x0A,send newline')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,\x0A,//send newline')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,md5sum '+tmpfilename+'.'+str(row["id"]))        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')        
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,rm '+tmpfilename+'.'+str(row["id"]))  
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')                  
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,md5sum '+filename+'.'+str(row["id"]))          
        count=count+1  
        sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')  
        count=count+1  

    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,cat '+filename+'.?? > '+filename)  
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')  
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,send,md5sum '+dumpDevice)  
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',REMOTE,expect,[# ],//wait for prompt')  
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,send,md5sum -b '+filename)  
    count=count+1 
    sequenceList=appendSequence(sequenceList,str(count)+',LOCAL,expect,[$ ],//wait for prompt')  
    count=count+1 
    
    return(sequenceList)
    
def func1(uartdevice, uartbaudrate, commandfile, debug):
    fnames=["sequenceid","endpoint","type","arg1","comment"]
    #sequenceid should start with 1 and always increment by 1
    #endpoint is either LOCAL or REMOTE
    #type is one of: send, expect, sleep
    #arg1 is:
    #    command for send
    #    pattern match for expect 
    #    number of seconds for sleep
    #comment is self-explained

    with open(commandfile) as csvfile:
        filereader=csv.DictReader(csvfile,fieldnames=fnames)
        sequencelist=[]
        for idx_row, row in enumerate(filereader):
            if row["type"]=="send":#translate the special characters
                if row["arg1"][0:4]=="\\x03":
                    row["arg1"]=chr(0x03)#ctrl-c
                elif row["arg1"][0:4]=="\\x0A":
                    row["arg1"]=chr(0x0A)#newline
                elif row["arg1"][0:4]=="\\x1A":
                    row["arg1"]=chr(0x1A)#ctrl-z
            if int(row["sequenceid"])>=0: #ignoThare rows with sequenceid=-1
                sequencelist.append(row)
    
    runSequence(uartdevice, uartbaudrate, sequencelist, debug)

def func2(uartDevice, uartBaudRate, dumpDevice, dumpSize, dumpChunkSize, dumpStartIndexChunk, dumpOutputDir, dumpOutputName, debug):
    #to query total size: # wc -c /dev/acta
    #acta is 20971520 bytes
    #actb is 157286400 bytes

    listChunks=genListChunk(dumpSize, dumpChunkSize, dumpStartIndexChunk)
    sequenceList=genSequence(uartDevice, uartBaudRate, dumpDevice, listChunks, dumpOutputDir, dumpOutputName)
    runSequence(uartDevice, uartBaudRate, sequenceList, debug)
       
def main(argc, argv):
    if argc < 3:
        usage()

    parser = argparse.ArgumentParser()
    parser.add_argument("--uartDevice", help="uart device on PC (e.g., /dev/ttyUSB0)", type=str, default="/dev/ttyUSB0")
    parser.add_argument("--uartBaudRate", help="uart baud rate", type=str, default="115200")
    parser.add_argument("--commandFile", help="name of file containing commands to execute", type=str)
    parser.add_argument("--dumpDevice", help="name of device on remote to dump (e.g., /dev/acta)", type=str)
    parser.add_argument("--dumpSize", help="total size of chunks", type=str)
    parser.add_argument("--dumpChunkSize", help="size of chunks in partial dumps", type=str, default="5M")
    parser.add_argument("--dumpStartIndexChunk", help="start index of first chunk to dump", type=str, default="0")
    parser.add_argument("--mode", help="func1 (uses commandFile) or func2 (generates commandFile)", type=str, default="func1")
    parser.add_argument("--dumpOutputDir", help="output directory for dumps", type=str, default="~/Downloads")
    parser.add_argument("--dumpOutputName", help="output filename prefix", type=str)    
    parser.add_argument("--debug", action="store_true", help="Activate debug mode")
      
    args = parser.parse_args()      
    
    if args.mode=="func1":    
        func1(args.uartDevice, args.uartBaudRate, args.commandFile, args.debug)
    elif args.mode=="func2":
        func2(args.uartDevice, args.uartBaudRate, args.dumpDevice, args.dumpSize, args.dumpChunkSize, args.dumpStartIndexChunk, args.dumpOutputDir, args.dumpOutputName, args.debug)
                
if __name__ == "__main__":
    main(len(sys.argv), sys.argv)


