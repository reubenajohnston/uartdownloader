import serial
import sys
import time
import traceback
import pexpect
    
cDELAY=0.05   
    
def open_client(device, baudrate, debug):
    #device="/dev/ttyUSB0"
    #baudrate=115200
    try:
        ser=serial.Serial(device,baudrate)
    
        sessionDict=dict([("serial",ser)])
    
        if(debug):
            print("open_client() "+str(sessionDict),flush=True)
        
        return(sessionDict)
    except Exception as e:
        print("Unable to connect to %s(%d)" %(device, baudrate))
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        sys.exit(1) #panic

def close_client(sessionDict, debug):
    try:
        ser=sessionDict["serial"]
        ser.close()
        sessionDict=None
        if(debug):
            print("close_client()",flush=True)       
        return(sessionDict)
    except Exception as e:
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        sys.exit(1) #panic

def wait_prompt(ser,prompt,debug):
    try:
        finished=False
        stdout=""
        if debug:
            print("Waiting for: %s\n" % (prompt.decode("utf8",errors="ignore")),flush=True)
        while not(finished):
            readcnt=ser.inWaiting()
            if (readcnt>0):
                data=ser.read(readcnt)
                temp=data.decode("utf8",errors="ignore")+"\n"
                if debug:
                    print("Received: \n%s" % (temp),flush=True)
                stdout=stdout+temp        
                if prompt.decode("utf8") in stdout:
                    finished=True
            else:
                time.sleep(cDELAY)
        return(stdout)
    except Exception as e:
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        traceback.print_exc()
        sys.exit(1) #panic
        
def serial_write(serial, bytecommand, debug):    
    if debug:
        print("Sending: \n"+bytecommand.decode("utf8",errors="ignore"),flush=True)
    serial.write(bytecommand)
    serial.flush()
    
def command_client(sessionDict, sequencestep, debug):
    #sends a single command
    ser=sessionDict["serial"]
        
    stdout=""
    if sequencestep["type"]=="send":
        bytecommand=(sequencestep["arg1"]+"\n").encode("utf8")
        serial_write(ser, bytecommand, debug)        
    elif sequencestep["type"]=="expect":
        temp=sequencestep["arg1"]
        if temp[1:3]=="# ":
            prompt=bytes("# ","utf8")#force a pattern for prompt
        elif temp[1:3]=="$ ":
            prompt=bytes("$ ","utf8")#force a pattern for prompt
        else:
            prompt=bytes(temp[1:-1],"utf8")#trim off leading '[' and trailing " ]"
        stdout=wait_prompt(ser, prompt, debug)
    elif sequencestep["type"]=="sleep":
        if debug:
            print("Sleeping "+sequencestep["arg1"]+" seconds\n")
        time.sleep(int(sequencestep["arg1"]))
    else:
        raise
         
    return(stdout)                

def command_handler(device,baudrate,session,function,sequencestep,debug,sessionDict):    
    if (session==False): #we are in single command mode
        sessionDict=open_client(device, baudrate, debug)
        stdout=command_client(sessionDict, sequencestep, debug)
        sessionDict=close_client(sessionDict, debug)
    else: #we are in session mode
        #if debug:
        #    print("session mode")
        if (function=="open_client"):
            sessionDict=open_client(device, baudrate, debug)
        elif (function=="command_client"):
            stdout=command_client(sessionDict,sequencestep,debug)
        elif (function=="close_client"):
            sessionDict=close_client(sessionDict, debug)
    return(sessionDict)
